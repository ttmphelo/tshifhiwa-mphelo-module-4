import 'package:flutter/material.dart';
import '../pages/login_page.dart';
import '../pages/register_page.dart';

class AuthPage extends StatefulWidget {
  const AuthPage({super.key});

  @override
  State<AuthPage> createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  bool showSecondPage = true;

  void toggleScreens() {
    setState(() {
      showSecondPage = !showSecondPage;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (showSecondPage) {
      return SecondPage(showRegisterPage: toggleScreens);
    } else {
      return RegisterPage(showSecondPage: toggleScreens);
    }
  }
}
