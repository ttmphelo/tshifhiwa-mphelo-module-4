// ignore_for_file: prefer_const_constructors, duplicate_ignore, unused_import

import 'package:finalquestion4/pages/home_page.dart';
import 'package:finalquestion4/splash_screen/splash_screen.dart';
import 'package:flutter/material.dart';

import "package:firebase_core/firebase_core.dart";

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SOH',
      home: ProfilePage(),
    );
  }
}
